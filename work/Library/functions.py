#-*- coding: utf-8 -*-
# pylint: disable=C0103
"""
Модуль с необходимыми функциями
"""
from tkinter import *
from configs import path_to_csv, columns_rus, genres, graphics, path_to_new_csv, columns_eng
import tkinter.ttk as ttk
from tkinter import messagebox as mb
import csv
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

new_csv = []
add_in_new_csv = []


def update_table(frame_table, var_pop_from, var_pop_to, var_dance_from, var_dance_to, var_temp_from,
           var_temp_to, var_energy_from, var_energy_to, selected):
    """
    Функция обновления таблицы с данными
    Входные данные:
        Фрейм, на котором размещается таблица
        Значения всех шкал отбора
        Список отобранных жанров
    Выходные данные: отсутствуют
    -----------------------
    Автор: Солдатов Семен

    """

    for widgets in frame_table.winfo_children():
        widgets.destroy()
    global new_csv
    choose(var_pop_from, var_pop_to, var_dance_from, var_dance_to, var_temp_from,
           var_temp_to, var_energy_from, var_energy_to, selected)
    show_table(frame_table)




def show_table(frame_table):
    """
    Функция, размещающая данные в таблице
    Входные данные:
        Фрейм, на котором размещается таблица
    Выходные данные: отсутствуют
    -----------------------
    Автор: Солдатов Семен

    """
    scrollbarx = Scrollbar(frame_table, orient=HORIZONTAL)
    scrollbary = Scrollbar(frame_table, orient=VERTICAL)
    global tree
    tree = ttk.Treeview(frame_table, columns=columns_rus, yscrollcommand=scrollbary.set,
                        xscrollcommand=scrollbarx.set, selectmode="extended", height=15)
    scrollbary.config(command=tree.yview)
    scrollbary.pack(side=RIGHT, fill=Y)
    scrollbarx.config(command=tree.xview)
    scrollbarx.pack(side=BOTTOM, fill=X)

    for i in columns_rus:
        tree.heading(f'{i}', text=f'{i}', anchor=W)

    tree.column("#0", minwidth=0, width=0)
    for i in range(1, 11):
        tree.column(f'#{i}', stretch=NO, minwidth=40, width=95)

    tree.pack()
    global new_csv
    for row in new_csv:
        genre = row[1]
        artist_name = row[2]
        track_name = row[3]
        popularity = row[4]
        acousticness = row[5]
        danceability = row[6]
        energy = row[7]
        key = row[8]
        loudness = row[9]
        mode = row[10]
        tempo = row[11]

        # 1-ый аргумент - идентификатор родителя
        # 2-ой аргумент показывает, что мы добавляем новую строку в конец
        tree.insert("", 0, values=(genre, artist_name, track_name, popularity, acousticness,
                                   danceability, energy, key, loudness,
                                   mode, tempo))


def add_item(genres_listbox, selected):
    """
    Функция, добавляющая данные в Листбокс
    Входные данные:
        Листбокс, содержащий все жанры
        Листбокс, содержащий выбранные жанры
    Выходные данные: отсутствуют
    -----------------------
    Автор: Самородин Михаил

    """
    select = list(genres_listbox.curselection())
    select.reverse()
    for i in select:
        if genres_listbox.get(i) not in selected.get(0, selected.size()):
            selected.insert(END, genres_listbox.get(i))


def delete_item(selected):
    """
    Функция, удаляющая данные из Листбокса
    Входные данные:
        Листбокс, содержащий выбранные жанры
    Выходные данные: отсутствуют
    -----------------------
    Автор: Самородин Михаил

    """
    select = list(selected.curselection())
    select.reverse()
    for i in select:
        selected.delete(i)


def choose(var_pop_from, var_pop_to, var_dance_from, var_dance_to, var_temp_from,
           var_temp_to, var_energy_from, var_energy_to, selected):
    """
    Функция, выбирающая нужные данные из csv файла
    Входные данные:
        Значения всех шкал отбора
        Список отобранных жанров
    Выходные данные: отсутствуют
    -----------------------
    Автор: Самородин Михаил

    """
    pop_from = int(var_pop_from.get())
    pop_to = int(var_pop_to.get())
    dance_from = float(var_dance_from.get())
    dance_to = float(var_dance_to.get())
    temp_from = float(var_temp_from.get())
    temp_to = float(var_temp_to.get())
    energy_from = float(var_energy_from.get())
    energy_to = float(var_energy_to.get())
    genres = selected.get(0, selected.size())
    global new_csv
    new_csv = []
    global add_in_new_csv
    if add_in_new_csv:
        for now in add_in_new_csv:
            new_csv.append(now)
    add_in_new_csv = []
    with open(path_to_csv, encoding='utf-8') as file:
        reader = csv.reader(file, delimiter=';')
        reader = list(reader)
    reader.pop(0)
    for str in reader:
         if pop_to >= float(str[4]) >= pop_from and dance_to >= float(str[6]) >= dance_from \
                 and temp_to >= float(str[11]) >= temp_from and energy_to >= float(str[7]) >= energy_from \
                 and (str[1] in genres or not genres):
             new_csv.append(str)


def download():
    """
    Функция, скачивающая выбранные данные в csv файл
    Входные данные: отсутствуют
    Выходные данные: отсутствуют
    -----------------------
    Автор: Солдатов Семен

    """
    global new_csv
    if new_csv:
        with open(path_to_new_csv, 'w', encoding='utf-8', newline='') as csvout:
            writer = csv.writer(csvout, delimiter=';')
            writer.writerow(columns_eng)
            for str in new_csv:
                writer.writerow(str)



def show_plot(combo_plot3, combo_plot1, combo_plot2, ):
    """
   Функция, размещающая выбранный график в новом окне
   Входные данные:
        3 коммобокса, дающие информацию о том
            1) какой график строить
            2) какую зависимость строить
   Выходные данные: отсутствуют
   -----------------------
   Автор: Алёшина Эллина
    """
    data = pd.read_csv(path_to_new_csv, encoding='utf-8', error_bad_lines='False', sep=';')
    if combo_plot3.get() != 'Выбрать' and 'Выбрать' != combo_plot1.get() != combo_plot2.get() != 'Выбрать':
        new_window = Toplevel()
        new_window.title('График')
        w = new_window.winfo_screenwidth()
        h = new_window.winfo_screenheight()
        w = w // 2  # середина экрана
        h = h // 2
        w = w - 300   # смещение от середины
        h = h - 300
        new_window.geometry('600x550+{}+{}'.format(w, h))
        if combo_plot3.get() == 'Диаграмма рассеивания':
            figure = plt.Figure(figsize=(6, 5.5))
            ax = figure.add_subplot(111)
            ax.set_title('Диаграмма рассеивания', color='green', size=20)
            ax.scatter(data[combo_plot1.get()], data[combo_plot2.get()])
            ax.set_xlabel(combo_plot1.get())
            ax.set_ylabel(combo_plot2.get())
            chart_type = FigureCanvasTkAgg(figure, new_window)
            chart_type.get_tk_widget().place(x=1, y=1)

        if combo_plot3.get() == 'Гистограмма':
            figure = plt.Figure(figsize=(6, 5.5))
            ax = figure.add_subplot(111)
            ax.set_title('Гистограмма', color='green', size=20)
            ax.hist(data[combo_plot2.get()])
            chart_type = FigureCanvasTkAgg(figure, new_window)
            chart_type.get_tk_widget().place(x=1, y=1)
            ax.set_xlabel(combo_plot1.get())

        if combo_plot3.get() == 'Столбчатая диаграмма':
            figure = plt.Figure(figsize=(6, 5.5))
            ax = figure.add_subplot(111)
            ax.set_title('Столбчатая диаграмма', color='green', size=20)
            ax.bar(data[combo_plot1.get()], data[combo_plot2.get()])
            chart_type = FigureCanvasTkAgg(figure, new_window)
            chart_type.get_tk_widget().place(x=1, y=1)
            ax.set_xlabel(combo_plot1.get())
            ax.set_ylabel(combo_plot2.get())

        if combo_plot3.get() == 'Диаграмма Бокса-Виксера':
            figure = plt.Figure(figsize=(6, 5.5))
            ax = figure.add_subplot(111)
            ax.set_title('Диаграмма Бокса-Виксера', color='green', size=20)
            ax.boxplot(data[combo_plot2.get()])
            chart_type = FigureCanvasTkAgg(figure, new_window)
            chart_type.get_tk_widget().place(x=1, y=1)
            ax.set_xlabel(combo_plot2.get())


def add(add_win, pop2, genre2, track2, artist2, dance2, energy2, loudness2, temp2, key2, acoust2, mode2):
    """
    Функция, добавляющая новый трек в дополнительный массив
    Входные данные:
        Окно добавления нового трека
        Данные, необходимые для нового трека
    Выходные данные: отсутствуют
    -----------------------
    Автор: Алёшина Эллина

    """
    global add_in_new_csv
    sring = '0.'
    # проверка данных на корректность
    if pop2.isdigit() and temp2.isdigit() and (dance2.startswith(sring) or float(dance2) == 1) and \
            (energy2.startswith(sring) or float(energy2) == 1) and (acoust2.startswith(sring) or float(acoust2) == 1) \
            and 100 >= int(pop2) > 0 and 1 >= float(dance2) > 0 and 1 >= float(energy2) > 0 \
            and 1 >= float(acoust2) > 0 and 0 >= float(loudness2) >= -40 and 250 >= float(temp2) > 0:
        add_in_new_csv.append(['', genre2, artist2, track2, pop2, acoust2, dance2, energy2, key2, loudness2, mode2, temp2])

    else:
        # ошибка, в случае введения некорректных данных
        mb.showerror('Ошибка', "Введены некоректные данные")
    add_win.destroy()


def show_adding():
    """
    Функция для ввода данных нового трека
    Входные данные: отсутствуют
    Выходные данные: отсутствуют
    -----------------------
    Автор: Самородин Михаил

    """
    pady = 5
    add_win = Toplevel()
    add_win.title("Добавление нового трека")
    w = add_win.winfo_screenwidth()
    h = add_win.winfo_screenheight()
    w = w // 2  # середина экрана
    h = h // 2
    w = w - 200  # смещение от середины
    h = h - 200
    add_win.geometry('400x400+{}+{}'.format(w, h))
    genres = ['Movie', 'R&B', 'A Capella', 'Alternative', 'Country', 'Dance', 'Jazz', 'Comedy', 'Soul', 'Soundtrack']
    keys = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#']

    pop1 = ttk.Label(add_win, text='Введите популярность (от 0 до 100):', font='Arial 10')
    pop2 = ttk.Entry(add_win)
    genre1 = ttk.Label(add_win, text='Выберите жанр:', font='Arial 10')
    genre2 = ttk.Combobox(add_win, values=genres, state="readonly", font='Arial 10')
    track1 = ttk.Label(add_win, text='Введите название песни:', font='Arial 10')
    track2 = ttk.Entry(add_win)
    artist1 = ttk.Label(add_win, text='Введите имя исполнителя:', font='Arial 10')
    artist2 = ttk.Entry(add_win)
    acoust1 = ttk.Label(add_win, text='Введите акустичность (от 0 до 1):', font='Arial 10')
    acoust2 = ttk.Entry(add_win)
    dance1 = ttk.Label(add_win, text='Введите танцевальность (от 0 до 1):', font='Arial 10')
    dance2 = ttk.Entry(add_win)
    energy1 = ttk.Label(add_win, text='Введите энергию (от 0 до 1):', font='Arial 10')
    energy2 = ttk.Entry(add_win)
    loudness1 = ttk.Label(add_win, text='Введите громкость (от -40 до 0):', font='Arial 10')
    loudness2 = ttk.Entry(add_win)
    temp1 = ttk.Label(add_win, text='Введите темп (от 0 до 250):', font='Arial 10')
    temp2 = ttk.Entry(add_win)
    key1 = ttk.Label(add_win, text='Выберите тональность:', font='Arial 10')
    key2 = ttk.Combobox(add_win, values=keys, state="readonly")
    mode1 = ttk.Label(add_win, text='Выберите лад:', font='Arial 10')
    mode2 = ttk.Combobox(add_win, values=['Major', 'Minor'], state="readonly", font='Arial 10')
    submit = ttk.Button(add_win, text='Подтвердить', command=lambda: add(add_win, pop2.get(), genre2.get(),
                                                            track2.get(), artist2.get(), dance2.get(), energy2.get(),
                                                              loudness2.get(), temp2.get(), key2.get(),
                                                              acoust2.get(), mode2.get()))

    pop1.grid(row=0, column=0, sticky='w', pady=pady)
    pop2.grid(row=0, column=1, sticky='e', pady=pady)
    temp1.grid(row=1, column=0, sticky='w', pady=pady)
    temp2.grid(row=1, column=1, sticky='e', pady=pady)
    track1.grid(row=2, column=0, sticky='w', pady=pady)
    track2.grid(row=2, column=1, sticky='e', pady=pady)
    artist1.grid(row=3, column=0, sticky='w', pady=pady)
    artist2.grid(row=3, column=1, sticky='e', pady=pady)
    acoust1.grid(row=4, column=0, sticky='w', pady=pady)
    acoust2.grid(row=4, column=1, sticky='e', pady=pady)
    dance1.grid(row=5, column=0, sticky='w', pady=pady)
    dance2.grid(row=5, column=1, sticky='e', pady=pady)
    energy1.grid(row=6, column=0, sticky='w', pady=pady)
    energy2.grid(row=6, column=1, sticky='e', pady=pady)
    loudness1.grid(row=7, column=0, sticky='w', pady=pady)
    loudness2.grid(row=7, column=1, sticky='e', pady=pady)
    genre1.grid(row=8, column=0, sticky='w', pady=pady)
    genre2.grid(row=8, column=1, sticky='e', pady=pady)
    key1.grid(row=9, column=0, sticky='w', pady=pady)
    key2.grid(row=9, column=1, sticky='e', pady=pady)
    mode1.grid(row=10, column=0, sticky='w', pady=pady)
    mode2.grid(row=10, column=1, sticky='e', pady=pady)
    submit.grid(row=11, column=0, columnspan=2, pady=10)


def delete_track():
    global tree
    row_id = tree.focus()
    tree.delete(row_id)
    ##treeview.delete(row_id)